#include "lcd_i2c.h"
#include "main.h"
#include <string.h>
//#include "stm32f1xx_hal.h"
//#include "stm32f1xx_hal_rcc.h"

//extern I2C_HandleTypeDef hi2c1;

lcdi2cType lcd;

extern uint8_t  i2c_send (uint8_t data);
//----------------------------------------------------------------
// ���������� �������
//----------------------------------------------------------------
void i2c_sda (uint8_t value)
{
	if(value == 0)
		HAL_GPIO_WritePin(SDA_GPIO_Port,SDA_Pin,GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(SDA_GPIO_Port,SDA_Pin,GPIO_PIN_SET);
}

void i2c_scl (uint8_t value)
{
	if(value == 0)
		HAL_GPIO_WritePin(SCL_GPIO_Port,SCL_Pin,GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(SCL_GPIO_Port,SCL_Pin,GPIO_PIN_SET);
}

void i2c_sda_input (void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

void i2c_sda_output (void)
{
  GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}
//----------------------------------------------------------------
// ����������� ��������
//----------------------------------------------------------------
void i2c_delay (uint32_t value)
{
	while(value > 1)
		value--;
}
//----------------------------------------------------------------
// ����������� ��������
//----------------------------------------------------------------
void lcd_delay(uint32_t time)
{
	while(time > 1)
	{
		time--;
	}
}
//----------------------------------------------------------------
// ����������� ���������� i2c ����������
//----------------------------------------------------------------
void i2c_start (void)
{
	i2c_sda(0);
	i2c_delay(100);
	i2c_scl(0);
	i2c_delay(100);
}
void i2c_stop (void)
{
	i2c_delay(100);
	i2c_scl(1);
	i2c_delay(100);
	i2c_sda(1);
}

uint8_t i2c_sendbyte (uint8_t data)
{
	uint8_t mask = 0x80;
	uint8_t index;
	uint8_t result = 0;
	
	for(index = 0; index < 8; index++)
	{
		i2c_delay(100);
		if((data & mask) != 0)
			i2c_sda(1);
		else
			i2c_sda(0);
		i2c_delay(100);
		i2c_scl(1);
		i2c_delay(200);
		i2c_scl(0);
		mask = mask>>1;
		
	}
	i2c_sda(0);
	i2c_sda_input();
	i2c_delay(50);
	i2c_scl(1);
	i2c_delay(200);
	result = HAL_GPIO_ReadPin(SDA_GPIO_Port,SDA_Pin);
	i2c_scl(0);
	i2c_sda_output();
	i2c_sda(0);
	return result;
}
uint8_t  i2c_send (uint8_t data)
{
	uint8_t res;
	i2c_start();
	i2c_sendbyte(lcd._Addr<<1);
	res = i2c_sendbyte(data);
	i2c_stop();
	return res;
}
void LCD_Print (char* string1, char* string2)
{
			lcd_clear();
			lcd_setCursor(0,0);
			lcd_print(string1);
			lcd_setCursor(0,1);
			lcd_print(string2);

}
//----------------------------------------------------------------
// �������� ����� �� I2C
//----------------------------------------------------------------
//void i2c_send (uint8_t data)
//{
//	//HAL_I2C_Master_Transmit(&hi2c1,lcd._Addr<<1,&data,1,2000);
//}
//----------------------------------------------------------------
// ��������  ����� � ����������
//----------------------------------------------------------------
void expanderWrite (uint8_t value)
{
	i2c_send(value | lcd._backlightval);
}
//----------------------------------------------------------------
// ��������� ������ ������
//----------------------------------------------------------------
void pulseEnable (uint8_t value)
{
	expanderWrite(value | LCD_EN);
	lcd_delay(500);
	expanderWrite(value & ~LCD_EN);
	lcd_delay(2);
	
}
//----------------------------------------------------------------
// �������� 4 ���
//----------------------------------------------------------------
void write4bits(uint8_t value)
{
	expanderWrite(value);
	pulseEnable(value);
}
//----------------------------------------------------------------
// �������� �����
//----------------------------------------------------------------
void lcd_send(uint8_t value, uint8_t mode)
{
	uint8_t highnib= value & 0xF0;
	uint8_t lownib = (value << 4) & 0xF0;
	write4bits((highnib) | mode);
	write4bits((lownib) | mode);
}
//----------------------------------------------------------------
// ������ � ������ lcd
//----------------------------------------------------------------
void lcd_write(uint8_t value)
{
	lcd_send(value,Rs);
}
//----------------------------------------------------------------
// �������� �������
//----------------------------------------------------------------
void lcd_command (uint8_t value)
{
	lcd_send(value,0);
}
//----------------------------------------------------------------
// �������� ������
//----------------------------------------------------------------
void lcd_printc (char value)
{
	lcd_send(value,1);
}
//----------------------------------------------------------------
// backlight on
//----------------------------------------------------------------
void backlight (void)
{
	lcd._backlightval=LCD_BACKLIGHT;
	expanderWrite(0);
}
//----------------------------------------------------------------
// backlight off
//----------------------------------------------------------------
void nobacklight (void)
{
	lcd._backlightval=LCD_NOBACKLIGHT;
	expanderWrite(0);
}
//----------------------------------------------------------------
// backlight on/off
//----------------------------------------------------------------
void lcd_backlight (uint8_t value)
{
	if(value)
		backlight();
	else
		nobacklight();
}
//----------------------------------------------------------------
// �������������
//----------------------------------------------------------------
void lcd_init(uint8_t adr,uint8_t lcd_cols, uint8_t lcd_rows)
{
	lcd._Addr = adr;
	lcd._cols = lcd_cols;
	lcd._rows = lcd_rows;
	lcd._backlightval = LCD_NOBACKLIGHT;
	lcd._displayfunction = LCD_4BITMODE | LCD_1LINE | LCD_5x8DOTS;
	
	if(lcd_rows > 1)
		lcd._displayfunction |= LCD_2LINE;
	
	lcd._numlines = lcd_rows;
	i2c_stop();
	
	HAL_Delay(50);
	
	// Now we pull both RS and R/W low to begin commands
	expanderWrite(lcd._backlightval);	// reset expanderand turn backlight off (Bit 8 =1)
	HAL_Delay(1000);

  	//put the LCD into 4 bit mode
	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46
	
	  // we start in 8bit mode, try to set 4 bit mode
   write4bits(0x03 << 4);
   HAL_Delay(5); // wait min 4.1ms
   
   // second try
   write4bits(0x03 << 4);
   HAL_Delay(5); // wait min 4.1ms
   
   // third go!
   write4bits(0x03 << 4);
   HAL_Delay(5);
   
   // finally, set to 4-bit interface
   write4bits(0x02 << 4);


	// set # lines, font size, etc.
	lcd_command(LCD_FUNCTIONSET | lcd._displayfunction);  
  HAL_Delay(5); // wait min 4.1ms

	// turn the display on with no cursor or blinking default
	lcd._displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
	lcd_display(1);
	
	// clear it off
	lcd_clear();
  HAL_Delay(5); // wait min 4.1ms	
	// Initialize to default text direction (for roman languages)
	lcd._displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;
	
	// set the entry mode
	lcd_command(LCD_ENTRYMODESET | lcd._displaymode);
  HAL_Delay(5); // wait min 4.1ms	
	lcd_home();
  HAL_Delay(5); // wait min 4.1ms
}
/********** high level commands, for the user! */
//----------------------------------------------------------------
// ������� ����������
//----------------------------------------------------------------
void lcd_clear(void)
{
	lcd_command(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
	HAL_Delay(20);  // this command takes a long time!
}

//----------------------------------------------------------------
// ������� �����
//----------------------------------------------------------------
void lcd_home(void)
{
	lcd_command(LCD_RETURNHOME);  // set cursor position to zero
	HAL_Delay(2);  // this command takes a long time!
}

//----------------------------------------------------------------
// ��������� �������
//----------------------------------------------------------------
void lcd_setCursor(uint8_t col, uint8_t row)
{
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > lcd._numlines ) 
	{
		row = lcd._numlines-1;    // we count rows starting w/0
	}
	lcd_command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

//----------------------------------------------------------------
// ���������� ��������
//----------------------------------------------------------------
void lcd_display(uint8_t value) 
{
	if(value != 0)
	{
		lcd._displaycontrol |= LCD_DISPLAYON;
		lcd_command(LCD_DISPLAYCONTROL | lcd._displaycontrol);
	}
	else
	{
		lcd._displaycontrol &= ~LCD_DISPLAYON;
		lcd_command(LCD_DISPLAYCONTROL | lcd._displaycontrol);
	}
}

// Turns the underline cursor on/off
//----------------------------------------------------------------
// ������������� �������
//----------------------------------------------------------------
void lcd_cursor(uint8_t value) 
{
	if(value != 0)
	{
		lcd._displaycontrol |= LCD_CURSORON;
		lcd_command(LCD_DISPLAYCONTROL | lcd._displaycontrol);
	}
	else
	{
		lcd._displaycontrol &= ~LCD_CURSORON;
		lcd_command(LCD_DISPLAYCONTROL | lcd._displaycontrol);
	}
}

// Turn on and off the blinking cursor
//----------------------------------------------------------------
// ��������
//----------------------------------------------------------------
void lcd_blink(uint8_t value) 
{
	if(value != 0)
	{
		lcd._displaycontrol |= LCD_BLINKON;
		lcd_command(LCD_DISPLAYCONTROL | lcd._displaycontrol);
	}
	else
	{
		lcd._displaycontrol &= ~LCD_BLINKON;
		lcd_command(LCD_DISPLAYCONTROL | lcd._displaycontrol);
	}
}

// These commands scroll the display without changing the RAM
void lcd_scrollDisplayLeft(void) 
{
	lcd_command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void lcd_scrollDisplayRight(void) 
{
	lcd_command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void lcd_leftToRight(void) 
{
	lcd._displaymode |= LCD_ENTRYLEFT;
	lcd_command(LCD_ENTRYMODESET | lcd._displaymode);
}

// This is for text that flows Right to Left
void lcd_rightToLeft(void) 
{
	lcd._displaymode &= ~LCD_ENTRYLEFT;
	lcd_command(LCD_ENTRYMODESET | lcd._displaymode);
}

// This will 'right justify' text from the cursor
void lcd_autoscroll(void) 
{
	lcd._displaymode |= LCD_ENTRYSHIFTINCREMENT;
	lcd_command(LCD_ENTRYMODESET | lcd._displaymode);
}

// This will 'left justify' text from the cursor
void lcd_noAutoscroll(void) 
{
	lcd._displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
	lcd_command(LCD_ENTRYMODESET | lcd._displaymode);
}

void lcd_createChar(uint8_t location, uint8_t charmap[]) 
{
	location &= 0x7; // we only have 8 locations 0-7
	lcd_command(LCD_SETCGRAMADDR | (location << 3));
	for (int i=0; i<8; i++) {
		lcd_write(charmap[i]);
	}
}
void lcd_print(char* data)
{
	uint8_t index = 0;
	while(data[index]!=0)
	{		
		lcd_printc(data[index]);
		index++;
	}
}


