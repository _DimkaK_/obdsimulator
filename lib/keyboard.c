/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : keyboard.c
  * @brief          : Keyboard 4x4 read
  ******************************************************************************
  * @attention
  *
  * 
  * ��� ������������ ������������ ���������� ���������� 4�4 ��� ��������� 
  * ������������ ������� � �������� ������� ������.
  * 
  *
  ******************************************************************************
  */#include "main.h"
#include "keyboard.h"


keyStruct Keys;

//------------------------------------------------------------------------------------------
// BSD ������� ��� ������ � ��������
//------------------------------------------------------------------------------------------
void keyDelay(uint16_t time)
{
	HAL_Delay(time);
}

void SetKeyOutput(uint8_t keyNum)
{
	switch(keyNum)
	{
		case 1:
						HAL_GPIO_WritePin(keyOut1_GPIO_Port,keyOut1_Pin,GPIO_PIN_RESET);
						HAL_GPIO_WritePin(keyOut2_GPIO_Port,keyOut2_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut3_GPIO_Port,keyOut3_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut4_GPIO_Port,keyOut4_Pin,GPIO_PIN_SET);
						break;
		case 2:
						HAL_GPIO_WritePin(keyOut1_GPIO_Port,keyOut1_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut2_GPIO_Port,keyOut2_Pin,GPIO_PIN_RESET);
						HAL_GPIO_WritePin(keyOut3_GPIO_Port,keyOut3_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut4_GPIO_Port,keyOut4_Pin,GPIO_PIN_SET);
						break;
		case 3:
						HAL_GPIO_WritePin(keyOut1_GPIO_Port,keyOut1_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut2_GPIO_Port,keyOut2_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut3_GPIO_Port,keyOut3_Pin,GPIO_PIN_RESET);
						HAL_GPIO_WritePin(keyOut4_GPIO_Port,keyOut4_Pin,GPIO_PIN_SET);
						break;
		case 4:
						HAL_GPIO_WritePin(keyOut1_GPIO_Port,keyOut1_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut2_GPIO_Port,keyOut2_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut3_GPIO_Port,keyOut3_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut4_GPIO_Port,keyOut4_Pin,GPIO_PIN_RESET);
						break;
		default:
						HAL_GPIO_WritePin(keyOut1_GPIO_Port,keyOut1_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut2_GPIO_Port,keyOut2_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut3_GPIO_Port,keyOut3_Pin,GPIO_PIN_SET);
						HAL_GPIO_WritePin(keyOut4_GPIO_Port,keyOut4_Pin,GPIO_PIN_SET);
						break;
	}
}

uint8_t GetKeyInput (void)
{
	uint8_t res = 0;
	if(HAL_GPIO_ReadPin(keyIn1_GPIO_Port,keyIn1_Pin) == GPIO_PIN_RESET)
		res += 1;
	if(HAL_GPIO_ReadPin(keyIn2_GPIO_Port,keyIn2_Pin) == GPIO_PIN_RESET)
		res += 2;
	if(HAL_GPIO_ReadPin(keyIn3_GPIO_Port,keyIn3_Pin) == GPIO_PIN_RESET)
		res += 3;
	if(HAL_GPIO_ReadPin(keyIn4_GPIO_Port,keyIn4_Pin) == GPIO_PIN_RESET)
		res += 4;

	return res;
}

//------------------------------------------------------------------------------------------
//  ������� ������� �� ����� �������.
//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
//  ������������� ��������� 
//------------------------------------------------------------------------------------------
void KeyInit(uint8_t downDelay)
{
	Keys.codeKey = 0;
	Keys.prevKey = 0;
	Keys.counterNoKey = 0;
	Keys.counterKeyDown = 0;
	Keys.counterLongDown = downDelay;
}

uint8_t ReadKey(void)
{
		uint8_t kOut = 0;
		uint8_t kIn = 0;;
		uint8_t codeKey = KEYNO;
	
		for(kOut = 1; kOut < 5; kOut++)
	  {
			
			SetKeyOutput(kOut);
			keyDelay(1);
			kIn = GetKeyInput();
			if(kIn != 0)
			{
				codeKey = (kOut-1) * 4 + kIn-1;
			}
		}
		return codeKey;
}
