#include "can_auto.h"
#include "main.h"
#include <string.h>


extern CAN_HandleTypeDef hcan;
extern void SerialPrint (char * report);
extern char report[50];
extern autoParamHeader TrackParam;

HAL_StatusTypeDef CAN_Data_Send(uint32_t stdID, uint32_t extID, uint8_t* buff)
{
	
	CAN_TxHeaderTypeDef Tx_Header;
	
	Tx_Header.StdId = stdID;
	Tx_Header.ExtId = extID;
	Tx_Header.RTR = CAN_RTR_DATA;
	Tx_Header.IDE = CAN_ID_STD;
	Tx_Header.DLC = 8;
	
	uint32_t mail_Box;

	
	return HAL_CAN_AddTxMessage(&hcan, &Tx_Header, buff, &mail_Box);
}

void CAN_ANSWER(uint8_t * buff)
{
	if(CAN_Data_Send(REPLY_ID,0,buff) == HAL_OK)
	{
//			sprintf(report,"can send ok, PID  %d,\r\n",buff[2]);
//			SerialPrint(report);
	}
	else
	{
			
		SerialPrint(report);
		HAL_CAN_Stop(&hcan);
		HAL_CAN_Start(&hcan);
		
	}
}

void mode1Answer (uint8_t recPID)
{
		uint8_t txbuff[8];
	
					switch(recPID)
					{
						case 0x00:
	
											txbuff[0] = 0x06;
											txbuff[1] = 0x41;// mode  = 0x40 + mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 0x80;
											txbuff[4] = 0x38;
											txbuff[5] = 0x00;
											txbuff[6] = 0x01;
											
											CAN_ANSWER(txbuff);
											break;
							
						case 0x05: // ����������� �������
											txbuff[0] = 0x03;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = TrackParam.temperature;
											CAN_ANSWER(txbuff);
											break;
						case 0x07: // �����
						case 0x09: // �����
											txbuff[0] = 0x03;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 158;
											CAN_ANSWER(txbuff);
											break;
						case 0x0F: // T ������������ �������
											txbuff[0] = 0x03;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 25 + 40;
											CAN_ANSWER(txbuff);
											break;
						case 0x10: // �������� ������ �������
											txbuff[0] = 0x04;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 14 + 40;
											CAN_ANSWER(txbuff);
											break;
						case 0x0C: // motor rate
											txbuff[0] = 0x04;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = (uint8_t)(TrackParam.rate / 64);
											txbuff[4] = (uint8_t)(TrackParam.rate % 64);
						
											CAN_ANSWER(txbuff);
						
//											sprintf(report,"%.2f rpm",(float)((recA*256 + recB)/4));
//											LCD_Print_OS("MOTOR RPM",report);
											break;
						case 0x0D:
											txbuff[0] = 0x03;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = (uint8_t)(TrackParam.speed);
						
											CAN_ANSWER(txbuff);
											break;
						case 0x20:
						case 0x60:
											txbuff[0] = 0x06;
											txbuff[1] = 0x41;// mode  = 0x40 + mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 0x80;
											txbuff[4] = 0x00;
											txbuff[5] = 0x00;
											txbuff[6] = 0x01;

											CAN_ANSWER(txbuff);
											break;
						case 0x40:
											txbuff[0] = 0x06;
											txbuff[1] = 0x41;// mode  = 0x40 + mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 0x80;
											txbuff[4] = 0x00;
											txbuff[5] = 0x00;
											txbuff[6] = 0x05;

											CAN_ANSWER(txbuff);
											break;
						
//						case 0x2F:
//											sprintf(report,"Full tank  %.2f %%\r\n",(float)(recA*100/256));
//											SerialPrint(report);
////											sprintf(report,"%.2f %% ",(float)(recA*100/256));
////											LCD_Print_OS("Full tank",report);
//											break;
						case 0x42:
											txbuff[0] = 0x04;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = (uint8_t)(TrackParam.voltage / 256);
											txbuff[4] = (uint8_t)(TrackParam.voltage % 256);

											CAN_ANSWER(txbuff);
											break;
						case 0x46:
											txbuff[0] = 0x03;
											txbuff[1] = 0x41;// mode
											txbuff[2] = recPID;// PID
											txbuff[3] = 24 + 40;
											CAN_ANSWER(txbuff);
											break;
//						case 0x5E:
//											sprintf(report,"������  %.2f �/�\r\n",(float)((recA*256 + recB)/20));
//											SerialPrint(report);
////											sprintf(report,"%.2f l/h ",(float)((recA*256 + recB)/20));
////											LCD_Print_OS("Engine fuel rate",report);
//											break;
						default:
											sprintf(report,"UNCNOW PID  0x%x in mode 1\r\n",recPID);
											SerialPrint(report);
//											LCD_Print_OS("UNCNOW PID","UNCNOW PID");
											break;
						
					}
	
}