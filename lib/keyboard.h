#ifndef __KEYBOARD_H
#define __KEYBOARD_H
#include "main.h"

#define KEY_ERR		255
#define KEYNO			255
#define KEY1			1
#define KEY2			2
#define KEY3			3
#define KEY4			4
#define KEY5			5
#define KEY6			6
#define KEY1L			11
#define KEY2L			12
#define KEY3L			13
#define KEY4L			14
#define KEY5L			15
#define KEY6L			16

typedef struct
{
	uint8_t codeKey;
	uint8_t prevKey;
	uint8_t counterKeyDown;
	uint8_t counterNoKey;
	uint8_t counterLongDown;
}keyStruct;

void KeyInit(uint8_t downDelay);
uint8_t ReadKey(void);

#endif // __KEYBOARD_H
