/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"


/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "lcd_i2c.h"
#include "can_auto.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

osThreadId defaultTaskHandle;
osThreadId canTransmitTaskHandle;
osThreadId canRecTaskHandle;
osThreadId buttonTaskHandle;
osThreadId userPortTaskHandle;
osThreadId lcdTaskHandle;
osMessageQId buttonQueueHandle;
osMessageQId userPortQueueHandle;
osMessageQId lcdQueueHandle;
/* USER CODE BEGIN PV */

char report[50];
uint32_t sendID;	
uint16_t errSend = 0;
uint16_t passSend = 0;
uint16_t errRec = 0;
autoParamHeader TrackParam;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
void StartDefaultTask(void const * argument);
void canTransmitStart(void const * argument);
void canRecStart(void const * argument);
void buttonStart(void const * argument);
void userPortStart(void const * argument);
void lcdTaskStart(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void SerialPrint (char * report)
{
	uint8_t userSize = strlen(report);
	HAL_UART_Transmit(&huart2,(uint8_t*)report,userSize,100);
}



HAL_StatusTypeDef CAN_Data_Receive(CAN_HandleTypeDef *hcan, CAN_RxHeaderTypeDef *pHeader, uint32_t RxFifo, uint8_t* buff)
{
	return  HAL_CAN_GetRxMessage(hcan, RxFifo, pHeader, buff);
}


void SendLCDQueue (lcdQueueRecord lcdC)
{
			xQueueSend(lcdQueueHandle, &lcdC.command, 100);
			for(uint8_t index = 0; index < sizeof(lcdC.arg);index++)
				xQueueSend(lcdQueueHandle, &lcdC.arg[index], 100);
}

BaseType_t ReceiveLCDQueue(lcdQueueRecord* lcdC)
{
//	BaseType_t result = pdFALSE;
//	BaseType_t res;
	uint8_t temp;
	
		if(xQueueReceive(lcdQueueHandle,&temp, portMAX_DELAY) != errQUEUE_EMPTY)
		{
				lcdC->command = temp;	
		}
		for(uint8_t index = 0; index < sizeof(lcdC->arg);index++)
		{
			if(xQueueReceive(lcdQueueHandle,&temp, portMAX_DELAY) != errQUEUE_EMPTY)
			{
					lcdC->arg[index] = temp;	
			}
		}
		return pdPASS;
}
void LCD_Print_OS (char* string1, char* string2)
{
			lcdQueueRecord lcdCommand;
	
			lcdCommand.command = COMLCD_CLR;
			SendLCDQueue(lcdCommand);
	
			lcdCommand.command = COMLCD_SETCURSOR;
			lcdCommand.arg[0] = 0;
			lcdCommand.arg[1] = 0;
			SendLCDQueue(lcdCommand);
	
			lcdCommand.command = COMLCD_PRN;
			strcpy((char*)lcdCommand.arg,string1);
			SendLCDQueue(lcdCommand);
	
			lcdCommand.command = COMLCD_SETCURSOR;
			lcdCommand.arg[0] = 0;
			lcdCommand.arg[1] = 1;
			SendLCDQueue(lcdCommand);

			lcdCommand.command = COMLCD_PRN;
			strcpy((char*)lcdCommand.arg,string2);
			SendLCDQueue(lcdCommand);

}


//HAL_StatusTypeDef HAL_CAN_GetRxMessage(CAN_HandleTypeDef *hcan, uint32_t RxFifo, CAN_RxHeaderTypeDef *pHeader, uint8_t aData[]);
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  MX_USART1_UART_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */
	

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of buttonQueue */
  osMessageQDef(buttonQueue, 8, uint8_t);
  buttonQueueHandle = osMessageCreate(osMessageQ(buttonQueue), NULL);

  /* definition and creation of userPortQueue */
  osMessageQDef(userPortQueue, 50, uint8_t);
  userPortQueueHandle = osMessageCreate(osMessageQ(userPortQueue), NULL);

  /* definition and creation of lcdQueue */
  osMessageQDef(lcdQueue, 120, uint8_t);
  lcdQueueHandle = osMessageCreate(osMessageQ(lcdQueue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of canTransmitTask */
  osThreadDef(canTransmitTask, canTransmitStart, osPriorityNormal, 0, 128);
  canTransmitTaskHandle = osThreadCreate(osThread(canTransmitTask), NULL);

  /* definition and creation of canRecTask */
  osThreadDef(canRecTask, canRecStart, osPriorityAboveNormal, 0, 128);
  canRecTaskHandle = osThreadCreate(osThread(canRecTask), NULL);

  /* definition and creation of buttonTask */
  osThreadDef(buttonTask, buttonStart, osPriorityNormal, 0, 128);
  buttonTaskHandle = osThreadCreate(osThread(buttonTask), NULL);

  /* definition and creation of userPortTask */
  osThreadDef(userPortTask, userPortStart, osPriorityNormal, 0, 128);
  userPortTaskHandle = osThreadCreate(osThread(userPortTask), NULL);

  /* definition and creation of lcdTask */
  osThreadDef(lcdTask, lcdTaskStart, osPriorityNormal, 0, 256);
  lcdTaskHandle = osThreadCreate(osThread(lcdTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  osKernelStart();
 
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN_Init(void)
{

  /* USER CODE BEGIN CAN_Init 0 */

  /* USER CODE END CAN_Init 0 */

  /* USER CODE BEGIN CAN_Init 1 */

  /* USER CODE END CAN_Init 1 */
  hcan.Instance = CAN1;
  hcan.Init.Prescaler = 3;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_1TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_10TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_5TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN_Init 2 */

  /* USER CODE END CAN_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(intLed_GPIO_Port, intLed_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, SCL_Pin|SDA_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin : intLed_Pin */
  GPIO_InitStruct.Pin = intLed_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(intLed_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : button_Pin */
  GPIO_InitStruct.Pin = button_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(button_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : SCL_Pin SDA_Pin */
  GPIO_InitStruct.Pin = SCL_Pin|SDA_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used 
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN 5 */
  /* Infinite loop */
  for(;;)
  {
		TrackParam.speed++;
		TrackParam.rate +=3;
		if (TrackParam.rate > 8000) 
			TrackParam.rate = 700;
		TrackParam.temperature++;
		TrackParam.voltage+=200;

    osDelay(500);
  }
  /* USER CODE END 5 */ 
}

/* USER CODE BEGIN Header_canTransmitStart */
/**
* @brief Function implementing the canTransmitTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_canTransmitStart */
void canTransmitStart(void const * argument)
{
  /* USER CODE BEGIN canTransmitStart */
	uint8_t buff[8];
	
	SerialPrint("Hello CAN\r\n\r\n\r\n");
	sendID = 1;


	uint8_t PID = 5;
	osDelay(5000);
	
  /* Infinite loop */
  for(;;)
  {
//		sendID = FUNCTIONAL_ID; //++; // = rand() & 0x1FF;
//		if(sendID > 0x1F0)
//			sendID = 1;
		for(char index = 4; index < 8; index ++)
		  buff[index] = index; //rand();
		buff[0] = 2;
		buff[1] = 1;
		buff[2] = PID;
		buff[3] = 0;

//		if(CAN_Data_Send(sendID,0x001,buff) == HAL_OK)
//		{
//				passSend++;
//				sprintf(report,"can send ok, PID  %d,\r\n",PID);
////				SerialPrint(report);
//		}
//		else
//		{
//				
//			errSend++;
//			sprintf(report,"can send FAIL, counter  %d/%d,\r\n",errSend,passSend);
//			passSend = 0;
//			SerialPrint(report);
//			HAL_CAN_Stop(&hcan);
//			HAL_CAN_Start(&hcan);
//			
//		}
		switch(PID)
		{
			case 0x05:
							PID = 0x0c;
							break;
			case 0x0C:
							PID = 0x0D;
							break;
			case 0x0D:
							PID = 0x2F;
							break;
			case 0x2F:
							PID = 0x42;
							break;
			case 0x42:
							PID = 0x5E;
							break;
			case 0x5E:
							PID = 0x05;
							break;
			default:
							PID = 5;
							break;
		}
		
		osDelay(1000);
  }
  /* USER CODE END canTransmitStart */
}

/* USER CODE BEGIN Header_canRecStart */
/**
* @brief Function implementing the canRecTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_canRecStart */
void canRecStart(void const * argument)
{
  /* USER CODE BEGIN canRecStart */
	
	CAN_RxHeaderTypeDef RxHeader;
	uint8_t rxbuff[8];
	uint8_t txbuff[8];
	
	uint8_t count;
	uint8_t recPID,recMode;
	uint8_t recA,recB,recC,recD;
	char rep1s[20];
	char rep2s[20];
	uint8_t recCounter = 0;

	CAN_FilterTypeDef sFilterConfig;
//	HAL_CAN_Receive_IT(&hcan1, CAN_FIFO0); //��� ��������� �������
	HAL_CAN_Start(&hcan);
	
	sFilterConfig.FilterBank = 0;
	sFilterConfig.FilterMode = CAN_FILTERMODE_IDMASK;
	sFilterConfig.FilterScale = CAN_FILTERSCALE_32BIT;
	sFilterConfig.FilterIdHigh = 0;
	sFilterConfig.FilterIdLow = 0x0000;
	sFilterConfig.FilterMaskIdHigh = 0;
	sFilterConfig.FilterMaskIdLow = 0x0000;
	sFilterConfig.FilterFIFOAssignment = 0;
	sFilterConfig.FilterActivation = ENABLE;
	sFilterConfig.SlaveStartFilterBank = 14;

	if (HAL_CAN_ConfigFilter(&hcan, &sFilterConfig) != HAL_OK)
	{
	/* Filter confikurotion Error */
		Error_Handler();
	}
	
  /* Infinite loop */
  for(;;)
  {

		count = HAL_CAN_GetRxFifoFillLevel(&hcan,CAN_RX_FIFO0);
//			sprintf(report,"count FIFO0  %d\r\n",count);
//			SerialPrint(report);
		if(count > 0)
		{
			HAL_GPIO_TogglePin(intLed_GPIO_Port,intLed_Pin);

			HAL_StatusTypeDef res = HAL_CAN_GetRxMessage(&hcan,CAN_RX_FIFO0,&RxHeader,rxbuff);
			if(res == HAL_OK)
			{
				  CAN_Data_Receive(&hcan,&RxHeader,CAN_RX_FIFO0,rxbuff);


					if(RxHeader.StdId != FUNCTIONAL_ID)
								break;

					recMode = rxbuff[1];
					recPID = rxbuff[2];
					recA = rxbuff[3];
					recB = rxbuff[4];
					recC = rxbuff[5];
					recD = rxbuff[6];
				
					sprintf(rep1s,"mode %d, PID 0x%X",recMode,recPID);
//					sprintf(rep2s,"0x%X/%X/%X/%X",rxbuff[0],rxbuff[1],recPID,recA);
					LCD_Print_OS(rep1s,"");
//					sprintf(report,"mode %d, PID 0x%X\r\n",recMode,recPID);
//					SerialPrint(report);

					switch(recMode)
					{
						case 1:
										mode1Answer(recPID);
										break;
						default:
										sprintf(report,"undefine MODE %d, PID 0x%X\r\n",recMode,recPID);
										SerialPrint(report);
										break;
					}
					

			}
		}
		count = HAL_CAN_GetRxFifoFillLevel(&hcan,CAN_RX_FIFO1);
		if(count > 0)
		{
			sprintf(report,"count FIFO1  %d\r\n",count);
			SerialPrint(report);
		}

		osDelay(10);
  }
  /* USER CODE END canRecStart */
}

/* USER CODE BEGIN Header_buttonStart */
/**
* @brief Function implementing the buttonTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_buttonStart */
void buttonStart(void const * argument)
{
  /* USER CODE BEGIN buttonStart */
  /* Infinite loop */
  for(;;)
  {
    osDelay(100);
  }
  /* USER CODE END buttonStart */
}

/* USER CODE BEGIN Header_userPortStart */
/**
* @brief Function implementing the userPortTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_userPortStart */
void userPortStart(void const * argument)
{
  /* USER CODE BEGIN userPortStart */
  /* Infinite loop */
  for(;;)
  {
    
		osDelay(300);
  }
  /* USER CODE END userPortStart */
}

/* USER CODE BEGIN Header_lcdTaskStart */
/**
* @brief Function implementing the lcdTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_lcdTaskStart */
void lcdTaskStart(void const * argument)
{
  /* USER CODE BEGIN lcdTaskStart */
  /* USER CODE BEGIN lcdTaskStart */
	char reportLCD[25];
	lcdQueueRecord commandLCD;
	
	lcd_init(0x3F,16, 2);
	lcd_backlight(1);
	lcd_print("Hello!!!!");
	//osDelay(1000);
	
  /* Infinite loop */
  for(;;)
  {
		if(ReceiveLCDQueue(&commandLCD) == pdPASS)
		{
				switch(commandLCD.command)
				{
					case COMLCD_CLR:
											lcd_clear();
									break;

					case COMLCD_HOME:
											lcd_home();
									break;
					
					case COMLCD_SETCURSOR:
											lcd_setCursor(commandLCD.arg[0],commandLCD.arg[1]);
									break;
					
					case COMLCD_PRN:
											strcpy(reportLCD,(char*)commandLCD.arg);
											lcd_print(reportLCD);
									break;

					case COMLCD_BACKLIGHT:
											lcd_backlight(commandLCD.arg[0]);
									break;

					default:
									break;
				}
				
		}
  }  /* USER CODE END lcdTaskStart */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
